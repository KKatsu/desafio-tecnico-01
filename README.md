Conforme pedido na task, criei uma API em uma linguagem (fiz em JS, utilizando o node.js) que retornasse uma string com o conteúdo "Hello World" na porta 80.

Criei o Dockerfile com base na própria documentação do node.js sobre como utilizar o Docker com a ferramenta https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

Devidamente "buildado", subi a imagem no dockerhub: https://hub.docker.com/repository/docker/kkatsuragii/desafio-tecnico

Rodando o container, na porta 80, retorna no navegador a mensagem pedida, ou rodando o comando 'curl -i localhost:80', que retorna o seguinte:

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: text/html; charset=utf-8
Content-Length: 12
ETag: W/"c-Lve95gjOVATpfV8EL5X4nxwjKHE"
Date: Fri, 08 Jul 2022 19:59:01 GMT
Connection: keep-alive
Keep-Alive: timeout=5

Hello World!