FROM node:alpine

WORKDIR /Documentos/desafio-tecnico-01

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 80

CMD ["npm", "start"]